Instruction to run SparkApp
===========================

## Prerequisites
  This project needs GNU/Linux OS, JDK version 8, maven, correctly settted variables JAVA_HOME and M2_HOME.
 Also yuo should download and install:
 
   *  [Kafka 2.11-1.0.0](https://www.apache.org/dyn/closer.cgi?path=/kafka/1.0.0/kafka_2.11-1.0.0.tgz)  
   *  [Hadoop 2.8.2](http://www.apache.org/dyn/closer.cgi/hadoop/common/hadoop-2.8.2/hadoop-2.8.2.tar.gz)  
   *  [Spark 2.2.0](https://www.apache.org/dyn/closer.lua/spark/spark-2.2.0/spark-2.2.0-bin-hadoop2.7.tgz)  
   *  [Cassandra 3.11.1](http://www.apache.org/dyn/closer.lua/cassandra/3.11.1/apache-cassandra-3.11.1-bin.tar.gz)  

## Steps
1. Set variable YARN\_CONF\_DIR to hadoop\_home\_dir/etc/hadoop/
2. Run script starthadoop.sh
3. Run script datagenerator.sh 100000. It will creates input data at file 'generateddata'.
4. Change direcotry to kafka home.
5. Run command `bash bin/zookeeper-server-start.sh -daemon config/zookeeper.properties`
6. Run command `bash kafka-server-start.sh -daemon config/server.propertie`
7. Cat the generateddata file and send the output to sparkrdd topik on kafka.  
   The command of this operation should be like   
   `cat spark_app_dir/generateddata | bin/kafka-console-producer.sh --broker-list localhost:9092 --topic sparkrdd`
8. Run comand `./cassandra -f` in cassandra home directory.
9. Run command `./cqlsh localhost` in cassandra home directory. It will open cassandra cqlsh terminal.
10. In cqlsh run command `create keyspace if not exists ks with replication = {'class':'SimpleStrategy', 'replication_factor':1};`. It will create keystore 'ks'
11. Also in cqlsh run command `create table if not exists ks.results (id int primary key, result text);` It will create table 'results'.
12. Change directory to app source folder.
13. Run command mvn clean package
14. Change direcory to spark home dir.
15. Run command `./bin/spark-submit --class sevumyan.mephi.spark.MainClass --master yarn  --deploy-mode cluster <Spark_app_dir>/target/hw2-spark-1.0-SNAPSHOT-jar-with-dependencies.jar`
