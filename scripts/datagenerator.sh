rm ../generateddata
i=1
range=100
salary=10000
abroads=10
while [ $i -le $1 ]
do
    j=1
    year=0
    
    while [ $year -lt 18 ]
    do
	year=$RANDOM
	let "year %= $range"	
    done	
    
    while [ $j -le 12 ]
    do
	rand=$RANDOM
	let "rand %= $range"

	if [ $rand -lt 80 ]
	then
	    lsalary=$salary
	    let "lsalary += $RANDOM"
	    echo $i, $j, $lsalary, $year >> ../generateddata
	else
            labroads=$RANDOM
	    let "labroads %=$abroads"
	    echo $i, $j, $labroads, $year >> ../generateddata
	fi
        j=$[$j+1]
    done
    i=$[$i+1]
done
