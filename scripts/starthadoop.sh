echo --------------------------------------------------------------------
echo Prepearing YARN
echo --------------------------------------------------------------------
export HADOOP_DIR=/home/robbie/Programs/hadoop/hadoop-2.8.1/
export PATH=$PATH:$HADOOP_DIR/bin:$HADOOP_DIR/sbin
systemctl start sshd
ssh-add
echo sshd started
hdfs namenode -format
echo hdfs formated
bash start-dfs.sh 
bash start-yarn.sh
hdfs dfsadmin -safemode leave
echo hadoop had been started
