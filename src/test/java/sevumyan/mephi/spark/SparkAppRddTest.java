package sevumyan.mephi.spark;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.kafka010.LocationStrategy;
import org.apache.spark.streaming.kafka010.OffsetRange;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import scala.Tuple2;

import java.util.*;

public class SparkAppRddTest extends Assert {
    private static JavaRDD<Result> results;

    private SparkAppRdd app = new SparkAppRddForTest();;

    @Test
    public void run() throws Exception {
        app.properties.setProperty("master-url","local");

        app.run();

        List<String> resultList = results.map(Result::getResult).collect();
        assertEquals(Arrays.asList(
                "Age category: YOUNG, count: 1, average salary: 11000,000000, average trips: 0,000000",
                "Age category: ADULT, count: 2, average salary: 2500,000000, average trips: 0,625000",
                "Age category: OLD, count: 1, average salary: 10000,000000, average trips: 1,250000"),
                resultList);
    }

    private class SparkAppRddForTest extends SparkAppRdd{

        SparkAppRddForTest(){
        }

        @Override
        protected JavaPairRDD<Integer, Record> getDataFromKafka(JavaSparkContext context, Map<String, Object> kafkaParams, OffsetRange[] ranges, LocationStrategy strategy) {
            List<Tuple2<Integer, Record>> objects = new ArrayList<>();
            objects.add(new Tuple2<>(1, new Record("1, 1, 120000, 18")));
            objects.add(new Tuple2<>(2, new Record("2, 2, 15, 35")));
            objects.add(new Tuple2<>(3, new Record("3, 3, 15, 70")));
            objects.add(new Tuple2<>(3, new Record("3, 4, 120000, 70")));
            objects.add(new Tuple2<>(1, new Record("1, 3, 12000, 18")));
            objects.add(new Tuple2<>(4, new Record("4, 3, 60000, 45")));
            return context.parallelizePairs(objects);
        }

        @Override
        protected void writeResultToCassandra(JavaRDD<Result> rdd) {
            SparkAppRddTest.results = rdd;
        }
    }
}
