package sevumyan.mephi.spark;

/**
 * Enumeration of age categories.
 */
public enum AgeCategory {
    YOUNG, ADULT, OLD;

    public static AgeCategory getCategory(int age) {
        if (age < 33) {
            return YOUNG;
        } else if (33 < age && age < 60) {
            return ADULT;
        } else return OLD;
    }
}
