package sevumyan.mephi.spark;

import java.io.Serializable;

/**
 * Entity class, which contains result information
 */
public class Result implements Serializable {
    private Integer id;
    private String result;

    /**
     * Standard constructor
     */
    Result() {

    }

    /**
     * Create result entity
     *
     * @param id     entity id
     * @param result string, which contains result information
     */
    Result(Integer id, String result) {
        this.id = id;
        this.result = result;
    }

    /**
     * Get entity id
     *
     * @return entity id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set entity id
     *
     * @param id entity i
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get result string
     *
     * @return string result
     */
    public String getResult() {
        return result;
    }

    /**
     * Set result string
     *
     * @param result result string
     */
    public void setResult(String result) {
        this.result = result;
    }
}
