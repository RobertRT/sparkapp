package sevumyan.mephi.spark;

import scala.Serializable;

/**
 * Entity class, which contains information about citizens.
 */
@SuppressWarnings("WeakerAccess")
public class Record implements Serializable {
    private static final String TO_STRING_FORMAT = "Pasport id: %d, Age: %d, Average Salary: %f, Average Approach: %f";

    private int passportId;
    private int month;
    private double salary;
    private double trips;
    private int age;
    private int count;

    /**
     * Create Record entity
     *
     * @param data data string
     */
    Record(String data) {
        String[] dataArray = data.replaceAll("\\s", "").split(",");
        this.passportId = Integer.parseInt(dataArray[0]);
        this.month = Integer.parseInt(dataArray[1]);
        this.salary = Integer.parseInt(dataArray[2]);
        this.trips = 0;
        if (salary < 1000) {
            trips = salary;
            salary = 0;
        }
        this.age = Integer.parseInt(dataArray[3]);
        count = 1;
    }

    /**
     * Get passport id
     *
     * @return passport id
     */
    public int getPassportId() {
        return passportId;
    }

    /**
     * Set passport id
     *
     * @param passportId passport id
     */
    public void setPassportId(int passportId) {
        this.passportId = passportId;
    }

    /**
     * Get month number
     *
     * @return month number
     */
    public int getMonth() {
        return month;
    }

    /**
     * Set month number
     *
     * @param month month number
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     * Get salary
     *
     * @return salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * Set salary
     *
     * @param salary salary
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * Get number of trips
     *
     * @return number of trips
     */
    public double getTrips() {
        return trips;
    }

    /**
     * Set number of trips
     *
     * @param trips number of trips
     */
    public void setTrips(double trips) {
        this.trips = trips;
    }

    /**
     * Get age
     *
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * Set age
     *
     * @param age age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Get count of citizens
     *
     * @return count of citizens
     */
    public int getCount() {
        return count;
    }

    /**
     * Set count of citizens
     *
     * @param count count of citizens
     */
    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return String.format(TO_STRING_FORMAT, passportId, age, salary, trips);
    }
}
