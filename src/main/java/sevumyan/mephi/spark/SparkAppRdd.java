package sevumyan.mephi.spark;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.apache.spark.streaming.kafka010.LocationStrategy;
import org.apache.spark.streaming.kafka010.OffsetRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Serializable;
import scala.Tuple2;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;

/**
 * Class, which implements business logic
 */
public class SparkAppRdd implements Serializable {
    private static final String APP_NAME = "DBHW2-KAFKA-SPARK-CASSANDRA";
    private static final String PROPERTIES_FILE = "app.properties";
    private static final String MASTER_URL_PROPERTY = "master-url";
    private static final String KAFKA_URL_PROPERTY = "kafka-url";
    private static final String KAFKA_TOPIC_PROPERTY = "kafka-topic";
    private static final String KAFK_OFFSET_RANGE_PROPERTY = "kafka-offset-range";
    private static final Logger LOGGER = LoggerFactory.getLogger(SparkAppRdd.class);

    protected Properties properties;

    /**
     * Standard constructor
     */
    public SparkAppRdd() {
        properties = loadProperties();
    }

    /**
     * Run spark app
     */
    public void run() {
        logStartApp();

        JavaSparkContext sparkContext = initSparkContext(properties.getProperty(MASTER_URL_PROPERTY));

        JavaRDD<Result> rdd = getDataFromKafka(
                sparkContext,
                initKafkaParams(properties.getProperty(KAFKA_URL_PROPERTY)),
                initOffsetRanges(properties.getProperty(KAFKA_TOPIC_PROPERTY),
                        Long.parseLong(properties.getProperty(KAFK_OFFSET_RANGE_PROPERTY))),
                LocationStrategies.PreferConsistent())
                .mapValues(record -> {
                    record.setSalary(record.getSalary() / 12);
                    record.setTrips(record.getTrips() / 12);
                    return record;
                })
                .reduceByKey((firstRecord, secondRecord) -> {
                    firstRecord.setSalary(firstRecord.getSalary() + secondRecord.getSalary());
                    firstRecord.setTrips(firstRecord.getTrips() + secondRecord.getTrips());
                    return firstRecord;
                })
                .mapToPair(record -> {
                    AgeCategory category = AgeCategory.getCategory(record._2.getAge());
                    return new Tuple2<>(category, record._2);
                })
                .reduceByKey((firstRecord, secondRecord) -> {
                    firstRecord.setCount(firstRecord.getCount() + secondRecord.getCount());
                    firstRecord.setSalary(firstRecord.getSalary() + secondRecord.getSalary());
                    firstRecord.setTrips(firstRecord.getTrips() + secondRecord.getTrips());
                    firstRecord.setAge(firstRecord.getAge() + secondRecord.getAge());
                    return firstRecord;
                })
                .mapValues(record -> {
                    record.setSalary(record.getSalary() / record.getCount());
                    record.setTrips(record.getTrips() / record.getCount());
                    record.setAge(record.getAge() / record.getCount());
                    return record;
                })
                .sortByKey()
                .map(x -> {
                    String result = String.format("Age category: %s, count: %d, average salary: %f, average trips: %f",
                            x._1.toString(), x._2.getCount(), x._2.getSalary(), x._2.getTrips());
                    return new Result(x._1.ordinal(), result);
                });
        rdd.foreach(result -> LOGGER.info("Result: {}", result.getResult()));
        writeResultToCassandra(rdd);

        logEndApp();
    }

    //=======================================================================================
    // Implementation
    //=======================================================================================


    JavaPairRDD<Integer, Record> getDataFromKafka(JavaSparkContext context, Map<String, Object> kafkaParams,
                                                  OffsetRange[] ranges, LocationStrategy strategy) {
        return KafkaUtils.createRDD(context, kafkaParams, ranges, strategy).mapToPair(kafkaData -> {
            Record rec = new Record((String) kafkaData.value());
            return new Tuple2<>(rec.getPassportId(), rec);
        });
    }

    void writeResultToCassandra(JavaRDD<Result> rdd) {
        javaFunctions(rdd).writerBuilder(properties.getProperty("cassandra-keyspace"),
                properties.getProperty("cassandra-tablename"), mapToRow(Result.class)).saveToCassandra();
    }


    private JavaSparkContext initSparkContext(String master) {
        SparkConf sparkConf = new SparkConf().setAppName(APP_NAME).setMaster(master);
        return new JavaSparkContext(sparkConf);
    }

    private Map<String, Object> initKafkaParams(String kafkaUrl) {
        Map<String, Object> params = new HashMap<>();
        params.put("bootstrap.servers", kafkaUrl);
        params.put("key.deserializer", StringDeserializer.class);
        params.put("value.deserializer", StringDeserializer.class);
        params.put("group.id", "sparktest_id");
        params.put("auto.offset.reset", "latest");
        params.put("enable.auto.commit", false);
        return params;
    }

    private OffsetRange[] initOffsetRanges(String topikName, long range) {
        OffsetRange[] offsetRanges = new OffsetRange[1];
        offsetRanges[0] = OffsetRange.apply(topikName, 0, 0, range);
        return offsetRanges;
    }

    private void logStartApp() {
        logBlockSeparation("Spark app started");
    }

    private void logEndApp() {
        logBlockSeparation("Spark app ended");
    }

    private void logBlockSeparation(String message) {
        LOGGER.info("-------------------------------------------------------------------------------------------");
        LOGGER.info(message);
        LOGGER.info("-------------------------------------------------------------------------------------------");
    }

    private Properties loadProperties() {
        Properties properties = new Properties();
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
            properties.load(stream);
        } catch (NullPointerException | IOException e) {
            throw new RuntimeException("Error in loading property file", e);
        }
        return properties;
    }
}
