package sevumyan.mephi.spark;

/**
 * App main class
 */
public class MainClass {

    /**
     * App main method.
     * @param args app input data
     */
    public static void main(String... args) {
        try {
            SparkAppRdd app = new SparkAppRdd();
            app.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
