### Homework 2. Big Data processing   a(1), b(1), c(2), d(1)

 a. Business logic:  
     1. Application that calculate average salary and abroad trips count statistics about citizens.  
	  Row input format: passport №, month, salary or passport №, month, number of abroad
      trips
      Row output format: age category, average salary, average number of abroad trips
	  
 b. Ingest technology:
    1. Kafka producer
 
 c. Storage technology:
    2. Cassandra
 
 d. Computation technology:
    1. Spark RDD
 
 Report includes:  
 1. ZIP-ed src folder with your implementation  
 2. Screenshot of successfully executed tests  
 3. Screenshots of successfully executed job and result (logs)  
 4. Quick build and deploy manual (commands, OS requirements etc)  
 5. System components communication diagram (UML or COMET)  
 
 
 In all cases bitbucket link (available for rovn\_m) must be provided in the report (report and src files must
 be send to mmrovnyagin@mephi.ru with DSBDA_HW2 topic).
 General criteria:  
   1. IDE agnostic build: Maven, Ant, Gradle, sbt, etc (10 points)  
   2. Unit tests are provided (20 points)  
   3. Code is well-documented (10 points)  
   4. Script for input file generation or calculation setup and control (10 points)  
   5. Working application that corresponds business logic, input/output format and additional  
      Requirements that has been started on cluster (30 points)  
   6. The relevant report was prepared (20 points)  
